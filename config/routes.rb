Medellin::Application.routes.draw do

  resources :periods, only: [:show]

  root :to => 'static#index'

  controller :static do
    get "now"
    get "future"
  end

  get "ajax/tweets"
  get "ajax/mentions"

  
  devise_for :admin_users

  get 'admin' => "admin#dashboard", as: :admin_user_root

  namespace :admin do
    resources :nodes
    resources :admin_users
    resources :categories
    resources :periods
    post 'upload' => 'nodes#upload', as: :import_nodes
  end
end
