require 'spec_helper'

describe AdminUser do
	it "has a valid factory" do
		FactoryGirl.create(:admin_user).should be_valid
	end

	it "isn't valid without a name" do
		FactoryGirl.build(:admin_user, first_name: nil, last_name: nil).should_not be_valid
	end
end