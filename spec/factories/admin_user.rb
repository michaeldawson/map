# Read about factories at https://github.com/thoughtbot/factory_girl



FactoryGirl.define do
	sequence :email do |n|
	  "email#{n}@factory.com"
	end

  factory :admin_user do
    first_name "test"
    last_name "test"
    email
    password "password"
  end
end
