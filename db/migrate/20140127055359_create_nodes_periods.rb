class CreateNodesPeriods < ActiveRecord::Migration
  def change
    create_table :nodes_periods, :id => false do |t|
        t.references :node
        t.references :period
    end
    add_index :nodes_periods, [:node_id, :period_id]
    add_index :nodes_periods, :node_id
  end
end
