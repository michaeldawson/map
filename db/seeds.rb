# encoding: utf-8

['facebook', 'twitter', 'website', 'google_plus'].each do |sl|
	SocialLinkPrefill.create(:name=>sl)
end

['now', 'in the future'].each do |t|
	Period.create(name: t)
end