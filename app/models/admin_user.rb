class AdminUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

  validates :first_name, presence: true
  validates :email, presence: true
  validates :password, presence: true
  
  has_many :nodes

  def full_name
  	"#{first_name} #{last_name}" || "Admin User"
  end
end
