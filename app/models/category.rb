class Category < ActiveRecord::Base

  has_many :nodes, dependent: :destroy

  def cat_class
  	name.parameterize.underscore
  end
end