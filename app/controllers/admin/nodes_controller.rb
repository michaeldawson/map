class Admin::NodesController < AdminController
  before_action :set_node, only: [:show, :edit, :update, :destroy]

  def show
    render 'edit'
  end

  def index
    @nodes = Node.all
  end

  def new
    @node = Node.new
  end

  def edit
  end

  def create
    @node = Node.new(node_params)

    respond_to do |format|
      if @node.save
        format.html { redirect_to [:admin, @node], notice: 'Node was successfully created.' }
        format.json { render action: 'show', status: :created, location: @node }
      else
        format.html { render action: 'new' }
        format.json { render json: @node.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @node.update(node_params)
        format.html { redirect_to [:admin, @node], notice: 'Node was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @node.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @node.destroy
    respond_to do |format|
      format.html { redirect_to admin_nodes_path }
      format.json { head :no_content }
    end
  end

  private
    def set_node
      @node = Node.find(params[:id])
    end

    def node_params
      params.require(:node).permit(:id, :description, :category_id, :name, {:period_ids => []})
    end
end
