class Admin::SiteConfigsController < AdminController
  before_action :set_site_config, only: [:update]

  # def index
  #   @site_configs = SiteConfig.all
  # end

  # def show
  # end

  def edit
    if params[:id] && SiteConfig.respond_to?(params[:id])
      @config = SiteConfig.send(params[:id])
      render 'admin/settings/index'
    else
      redirect_to admin_path
    end
  end
  
  def update
    if @site_config.update(site_config_params)
      redirect_to admin_path, notice: 'SiteConfig was successfully updated.'
    else
      redirect_to :back
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site_config
      @site_config = SiteConfig.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def site_config_params
      params.require(:site_config).permit(:id, :name, :description, settings_attributes: [:id, :key, :value])
    end
end
