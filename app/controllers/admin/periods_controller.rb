class Admin::PeriodsController < AdminController
  before_action :set_period, only: [:show, :edit, :update, :destroy]

  def show
    render 'edit'
  end

  def index
    @periods = Period.all
  end

  def new
    @period = Period.new
  end

  def edit
  end

  def create
    @period = Period.new(period_params)

    respond_to do |format|
      if @period.save
        format.html { redirect_to [:admin, @period], notice: 'Period was successfully created.' }
        format.json { render action: 'show', status: :created, location: @period }
      else
        format.html { render action: 'new' }
        format.json { render json: @period.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @period.update(period_params)
        format.html { redirect_to [:admin, @period], notice: 'Period was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @period.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @period.destroy
    respond_to do |format|
      format.html { redirect_to admin_periods_path }
      format.json { head :no_content }
    end
  end

  private
    def set_period
      @period = Period.find_by_name(params[:id])
    end

    def period_params
      params.require(:period).permit(:id, :name)
    end
end
