class AdminController < ApplicationController
before_filter :authenticate_admin_user!

  def dashboard
    @node_count = Node.count
  end

end
