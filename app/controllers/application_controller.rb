class ApplicationController < ActionController::Base
  protect_from_forgery

  before_action :layout_stylesheet
  layout :layout_by_controller


  def layout_stylesheet

    top_class = !(self.class.parent.to_s == "Object") ? self.class.parent.to_s : self.class.to_s
  	case top_class
  	when "Devise"
  		@stylesheet="admin"
  	when "AdminController", "Admin"
  		@stylesheet="admin"
  	else
  		@stylesheet="application"
  	end

  end


  def layout_by_controller
    top_class = !(self.class.parent.to_s == "Object") ? self.class.parent.to_s : self.class.to_s

    case top_class
    when "Devise"
      "devise"
    when "AdminController", "Admin"
      "admin"
    else
      "application"
    end
  end


end