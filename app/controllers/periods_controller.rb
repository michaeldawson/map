class PeriodsController < ApplicationController
	before_action :set_period 

  def show
  	@overlay_color = "white" 
    @background_img=1
    @header = "Medellín #{@period.name}"

    @nodes = @period.nodes
    @categories = Category.all
    @targets = Target.all
    
    @palette=["d06e39","#bbb", "#50b1a0", "#50b189", "#F26B6B", "#d04275", "#d68f2a", "#d06e39", "#4971A6"].reverse

  end

private
  def set_period
    @period = Period.find_by_name(params[:id])
  end

  def period_params
    params.require(:period).permit(:id)
  end
end
