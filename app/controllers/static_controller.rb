class StaticController < ApplicationController

  def index
    redirect_to period_path(Period.first)
  end

end

