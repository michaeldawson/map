APP = {
  common: {
    init: function() {
      $('#pt-main').fadeIn('slow');
      App.init();
    }
  },
};

UTIL = {
  init: function() {
    var body = document.body,
        controller = body.getAttribute( "data-controller" ),
        action = body.getAttribute( "data-action" );
 
    UTIL.exec( "common" );
    UTIL.exec( controller );
    UTIL.exec( controller, action );
  },

  exec: function( controller, action ) {
    var ns = APP,
        action = ( action === undefined ) ? "init" : action;
 
    if ( controller !== "" && ns[controller] && typeof ns[controller][action] == "function" ) {
      ns[controller][action]();
    }
  },
};
 
$(window).bind('page:change', UTIL.init );